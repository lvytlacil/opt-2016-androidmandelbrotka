# INFO #

An app that visualises the mandelbrot set using a simple escape time algorithm on a pre-generated color palette. It allows zooming, sharing and saving the current views.

![Snímek obrazovky pořízený 2017-01-30 02-03-46.png](https://bitbucket.org/repo/9Ljq49/images/1988647776-Sn%C3%ADmek%20obrazovky%20po%C5%99%C3%ADzen%C3%BD%202017-01-30%2002-03-46.png)


The project was created in Android Studio using the Android SDK Build-tools 24.0.1.