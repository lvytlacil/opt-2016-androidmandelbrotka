package com.edu.goffykenni.mandelbrotka.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.edu.goffykenni.mandelbrotka.infrastructure.MandelbrotkaApplication;

/**
 * Created by Libor on 3. 9. 2016.
 */
public class BaseActivity extends AppCompatActivity {
    protected MandelbrotkaApplication application;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (MandelbrotkaApplication) getApplication();
    }
}
