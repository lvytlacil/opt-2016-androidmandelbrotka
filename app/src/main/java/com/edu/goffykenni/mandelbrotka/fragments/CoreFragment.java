package com.edu.goffykenni.mandelbrotka.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.edu.goffykenni.mandelbrotka.R;
import com.edu.goffykenni.mandelbrotka.util.CartesianRect;
import com.edu.goffykenni.mandelbrotka.views.MandelbrotView;

import java.io.File;
import java.io.FileOutputStream;

/**
 * The core fragment, that displays the mandelbrot set view and allows to save
 * and restore its state.
 */
public class CoreFragment extends Fragment {
    private static final String BUNDLE_KEY_MANDELBROT_SEGMENT = "BUNDLE_KEY_MANDELBROT_SEGMENT";
    private static final String BUNDLE_KEY_MANDELBROT_RATIO_FIXED = "BUNDLE_KEY_MANDELBROT_RATIO_FIXED";
    private static final String BUNDLE_KEY_MANDELBROT_BASE_COLOR = "BUNDLE_KEY_MANDELBROT_BASE_COLOR";
    private MandelbrotView mMandelbrotView;

    private Toast imageToast = null;
    private boolean isChecked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_core, container, false);
        mMandelbrotView = (MandelbrotView) result.findViewById(R.id.fragment_core_mandelbrotView);
        mMandelbrotView.setOnTouchListener(mMandelbrotView);

        if (savedInstanceState != null) {
            CartesianRect segment = savedInstanceState.getParcelable(BUNDLE_KEY_MANDELBROT_SEGMENT);
            boolean ratioFixed = savedInstanceState.getBoolean(BUNDLE_KEY_MANDELBROT_RATIO_FIXED);
            int[] baseColor = savedInstanceState.getIntArray(BUNDLE_KEY_MANDELBROT_BASE_COLOR);
            isChecked = ratioFixed;
            if (segment != null) {
                mMandelbrotView.setMandelbrotSegment(segment);
            }
            mMandelbrotView.setRatioFixed(ratioFixed);
            if (baseColor != null) {
                mMandelbrotView.setBaseColor(baseColor[0], baseColor[1], baseColor[2]);
            }
        }
        return result;
    }

    @Override
    public void onStart() {
        mMandelbrotView.restartIfCancelled();
        super.onStart();
    }

    @Override
    public void onStop() {
        mMandelbrotView.stop();
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BUNDLE_KEY_MANDELBROT_SEGMENT, mMandelbrotView.getMandelbrotSegment());
        outState.putBoolean(BUNDLE_KEY_MANDELBROT_RATIO_FIXED, mMandelbrotView.isRatioFixed());
        outState.putIntArray(BUNDLE_KEY_MANDELBROT_BASE_COLOR, mMandelbrotView.getBaseColor());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_core, menu);
        menu.findItem(R.id.menu_fragment_core_fixRatio).setChecked(isChecked);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_fragment_core_reset) {
            mMandelbrotView.resetView();
        } else if (item.getItemId() == R.id.menu_fragment_core_fixRatio) {
            item.setChecked(!item.isChecked());
            mMandelbrotView.changeRatio(item.isChecked());
        } else if (item.getItemId() == R.id.menu_fragment_core_recolor) {
            mMandelbrotView.recolor();
        } else if (item.getItemId() == R.id.menu_fragment_core_share) {
            shareBitmap();
        } else if (item.getItemId() == R.id.menu_fragment_core_save) {
            saveBitmap();
        }
        return true;
    }

    /**
     * Send a request to the view to save the current bitmap and shows a toast message with result.
     */
    private void saveBitmap() {
        if (imageToast != null)
            imageToast.cancel();

        MandelbrotView.MandelbrotBitmapShareResult res = mMandelbrotView.saveBitmap(getActivity());
        switch (res) {
            case ERROR:
                imageToast = Toast.makeText(getActivity(), "Error during saving.", Toast.LENGTH_SHORT);
                imageToast.show();
                break;
            case NOT_READY:
                imageToast = Toast.makeText(getActivity(), "Image is not ready.", Toast.LENGTH_SHORT);
                imageToast.show();
                break;
            case OK:
                imageToast = Toast.makeText(getActivity(), "Image saved successfully.", Toast.LENGTH_SHORT);
                imageToast.show();
                break;
        }
    }

    /**
     * Sends a request to share the current bitmap and shows a toast message with the result
     */
    private void shareBitmap() {
        if (imageToast != null)
            imageToast.cancel();

        switch (mMandelbrotView.shareBitmap(getActivity())) {
            case ERROR:
                imageToast = Toast.makeText(getActivity(), "Error during sharing.", Toast.LENGTH_SHORT);
                imageToast.show();
                break;
            case NOT_READY:
                imageToast = Toast.makeText(getActivity(), "Image is not ready", Toast.LENGTH_SHORT);
                imageToast.show();
                break;
        }
    }
}
