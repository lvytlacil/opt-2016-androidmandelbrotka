package com.edu.goffykenni.mandelbrotka.util;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a rectangle given by a position of its top-left and bottom-right corners in
 * cartesian coordinates. This class is immutable. In order to be able to save this class
 * inside a bundle, Parcelable interface is implemented.
 */
public class CartesianRect implements Parcelable{
    public final float left;
    public final float top;
    public final float right;
    public final float bottom;
    public final float width;
    public final float height;

    /**
     * Creates a new cartesian rectangle with the given params, but performs no check, so
     * you should ensure that left <= right and bottom <= top
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public CartesianRect(float left, float top, float right, float bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        width = right - left;
        height = top - bottom;
    }

    /* Parceable implementation */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(left);
        parcel.writeFloat(top);
        parcel.writeFloat(right);
        parcel.writeFloat(bottom);
        parcel.writeFloat(width);
        parcel.writeFloat(height);
    }

    public static final Parcelable.Creator<CartesianRect> CREATOR =
            new ClassLoaderCreator<CartesianRect>() {
                @Override
                public CartesianRect createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    return new CartesianRect(parcel);
                }

                @Override
                public CartesianRect createFromParcel(Parcel parcel) {
                    return new CartesianRect(parcel);
                }

                @Override
                public CartesianRect[] newArray(int size) {
                    return new CartesianRect[size];
                }
            };

    private CartesianRect(Parcel parcel) {
        left = parcel.readFloat();
        top = parcel.readFloat();
        right = parcel.readFloat();
        bottom = parcel.readFloat();
        width = parcel.readFloat();
        height = parcel.readFloat();
    }
}
