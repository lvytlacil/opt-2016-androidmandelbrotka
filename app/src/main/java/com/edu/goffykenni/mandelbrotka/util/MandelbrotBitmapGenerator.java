package com.edu.goffykenni.mandelbrotka.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

/**
 * Class used to generate the bitmap of a segment of mandelbrot set on using a working thread.
 */
public class MandelbrotBitmapGenerator extends AsyncTask<MandelbrotBitmapGenerator.MandelbrotGeneratorParams,
        Float, Bitmap> {
    private static final float TEXT_SIZE_DP = 10.0f;
    private final MandelbrotGeneratorListener callBacks;
    private final Context context;
    private final float textScale;
    private final Paint paint;
    private final Rect cachedTextBounds;

    public MandelbrotBitmapGenerator(MandelbrotGeneratorListener listener, Context context) {
        this.callBacks = listener;
        this.context = context;
        this.textScale = context.getResources().getDisplayMetrics().density;
        this.paint = new Paint();
        cachedTextBounds = new Rect();
    }

    /**
     * Gets the color for the given point using a simple escape time algorithm.
     * The point gets its color assigned according to how quickly it reaches the escape condition (its
     * distance form the origin is 2 or greater, because no mandelbrot set point exists with such
     * properties)
     * @param cx
     * @param cy
     * @param palette
     * @return
     */
    private int getColor(float cx, float cy, int[] palette) {
        float x = 0.0f;
        float y = 0.0f;
        int it = 0, maxIterations = palette.length;
        float xTemp, xx = x * x, yy = y * y;
        while (it < maxIterations - 1 && xx + yy < 2 * 2) {
            xTemp = x;
            x = xx - yy + cx;
            y = 2 * xTemp * y + cy;
            xx = x * x;
            yy = y * y;
            it++;
        }
        return palette[it];
    }

    private Bitmap generateBitmap(MandelbrotGeneratorParams params) {
        CartesianRect segment = params.mandelbrotSegment;
        Rect dimensions = params.dimensions;

        String textLeftTop = String.format("left-top: [%.6f, %.6f]", segment.left, segment.top);
        String textRightBottom = String.format("right-bottom: [%.6f, %.6f]", segment.right, segment.bottom);

        // Determine if the text-width is small enough to fit on screen, otherwise
        // we will draw it on two rows
        int textSizePixel = dpToPixels(TEXT_SIZE_DP);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setColor(Color.WHITE);
        paint.setTextSize(textSizePixel);
        paint.getTextBounds(textLeftTop + textRightBottom, 0,
                textLeftTop.length() + textRightBottom.length(), cachedTextBounds);
        boolean singleLine = cachedTextBounds.width() <= dimensions.width();
        int verticalBlankSpace = singleLine ? 2 * textSizePixel : 3 * textSizePixel;

        // The drawing of the mandelbrot segment, this is the computationally difficult part
        Bitmap result = drawMandelbrotSegment(params.palette, segment, dimensions, verticalBlankSpace);

        // Add the text info
        Canvas canvas = new Canvas(result);
        if (singleLine)
            canvas.drawText(textLeftTop + "; " + textRightBottom, textSizePixel, result.getHeight() - textSizePixel / 2, paint);
        else {
            canvas.drawText(textLeftTop, textSizePixel, result.getHeight() - 2 * textSizePixel, paint);
            canvas.drawText(textRightBottom, textSizePixel, result.getHeight() - textSizePixel, paint);
        }

        return result;
    }

    private Bitmap drawMandelbrotSegment(int[] palette, CartesianRect segment,
                                         Rect dimensions, int verticalBlankSpace) {
        Bitmap result = Bitmap.createBitmap(dimensions.width(), dimensions.height() + verticalBlankSpace,
                Bitmap.Config.RGB_565);

        float updateNotificationStep = dimensions.width() / 10;
        final float xStep = segment.width / dimensions.width();
        final float yStep = segment.height / dimensions.height();
        float x, y;

        /* Drawing column-wise, i.e columns from left to right and each column from top to bottom */
        x = segment.left;
        for (int col = 0; col < dimensions.width(); col++) {
            y = segment.top;
            for (int row = 0; row < dimensions.height(); row++) {
                result.setPixel(col, row, getColor(x, y, palette));
                y -= yStep;
            }
            if (isCancelled())
                return result;
            x += xStep;
            if (col % updateNotificationStep == 0)
                publishProgress((float) col / dimensions.width());
        }
        return result;
    }


    private int dpToPixels(float dp) {
        // Add 0.5 so it round to the nearest integer when the cast is performed.
        return (int) (dp * textScale + 0.5);
    }

    @Override
    protected void onPreExecute() {
        callBacks.onPreExecute();
    }

    @Override
    protected Bitmap doInBackground(MandelbrotGeneratorParams... transformations) {
        return generateBitmap(transformations[0]);
    }

    @Override
    protected void onProgressUpdate(Float... values) {
        callBacks.onUpdate(values[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        callBacks.onPostExecute(bitmap);
    }

    @Override
    protected void onCancelled(Bitmap bitmap) {
        callBacks.onCancelled(bitmap);
    }

    /**
     * A carriage for passing mandelbrot segment and the view dimensions
     */
    public static class MandelbrotGeneratorParams {
        final Rect dimensions;
        final CartesianRect mandelbrotSegment;
        final int palette[];

        public MandelbrotGeneratorParams(Rect dimensions, CartesianRect mandelbrotSegment,
                     int palette[]) {
            this.dimensions = dimensions;
            this.mandelbrotSegment = mandelbrotSegment;
            this.palette = palette;
        }
    }

    public interface MandelbrotGeneratorListener {
        void onPreExecute();
        void onUpdate(float progress);
        void onPostExecute(Bitmap result);
        void onCancelled(Bitmap result);
    }
}
