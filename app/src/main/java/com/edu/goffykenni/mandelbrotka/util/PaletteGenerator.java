package com.edu.goffykenni.mandelbrotka.util;

import android.graphics.Color;

/**
 * Class that can be used to generate an array simulating a continuous-like color palette
 */
public final class PaletteGenerator {
    /**
     * Generates a new continuous-like palette based on the passed rgb values and stores
     * it into the passed array. All three values (red, green, blue) are valid in the range 0-255,
     * but if a value outside this range is entered, it will be automatically corrected.
     * @param red The red part
     * @param green The green part
     * @param blue The blue part
     * @param palette The array the palette should be generated into. It must be non-null array
     *                and can have any length - the length simply determines the size of the palette.
     *
     */
    public static void createPalette(int red, int green, int blue, int[] palette) {
        // ping booleans assures, that all values changes each iteration and stays within
        // the 0 - 255 range
        boolean pingRed = false, pingGreen = false, pingBlue = false;
        for (int i = 0; i < palette.length; i++) {
            red = pingRed ? red + 2 : red - 2;
            green = pingGreen ? green + 3 : green - 3;
            blue = pingBlue ? blue + 5 : blue - 5;

            if (red > 255) {
                red = 255;
                pingRed = false;
            }
            if (red < 0) {
                red = 0;
                pingRed = true;
            }

            if (green > 255) {
                green = 255;
                pingGreen = false;
            }
            if (green < 0) {
                green = 0;
                pingGreen = true;
            }

            if (blue > 255) {
                blue = 255;
                pingBlue = false;
            }
            if (blue < 0) {
                blue = 0;
                pingBlue = true;
            }

            palette[i] = Color.rgb(red, green, blue);
        }
    }
}
