package com.edu.goffykenni.mandelbrotka.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.edu.goffykenni.mandelbrotka.R;
import com.edu.goffykenni.mandelbrotka.util.CartesianRect;
import com.edu.goffykenni.mandelbrotka.util.MandelbrotBitmapGenerator;
import com.edu.goffykenni.mandelbrotka.util.PaletteGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Represents a rectangular area that is responsible for drawing the mandelbrot fractal.
 */
public class MandelbrotView extends View implements View.OnTouchListener, MandelbrotBitmapGenerator.MandelbrotGeneratorListener{
    private static final String TAG = "MANDL";
    // The default segment of mandelbrot set
    private static final CartesianRect INITIAL_SEGMENT =
            new CartesianRect(-2.0f, 1.0f, 1.0f, -1.0f);
    // Size of the color palette. Lower number means less precision, but faster computation (linearly)
    private static final int PALETTE_SIZE = 256;
    // The initial color on which the palette will be based
    private static final int[] INITIAL_RGB_COLOR = new int[] {0, 0, 0};

    // Represents the actual segment of mandelbrot set to be drawn
    private CartesianRect mandelbrotSegment = null;
    // Represents the view's dimensions
    private Rect dimensions = null;

    // If the ratio is fixed
    private boolean isRatioFixed = false;
    // Cached screen center
    private int centerX;
    private int centerY;
    // Bitmap for storing the drawing before it is shown to the user
    private Bitmap currentBitmap = null;
    private boolean isBitmapValid;
    // Paint object used to hold draw settings
    private Paint paint;
    // The current color palette
    PaletteCariage paletteCariage;
    // Whether a background thread is currently created and working
    private WorkingThreadState workingThreadState = WorkingThreadState.INACTIVE;
    // Whether there was a request to generate new bitmap while other request was being processed
    private boolean isPendingRequest = false;
    // Current BitmapGenerator
    private MandelbrotBitmapGenerator mandelbrotBitmapGenerator;
    // Represents the percentage of the work already done by the background thread
    private float progress;
    // Zoom toast reference
    private Toast zoomMessageToast = null;
    private Random random = new Random();

    /*--- Getters and Setters ---*/

    public CartesianRect getMandelbrotSegment() {
        return mandelbrotSegment;
    }

    public void setMandelbrotSegment(CartesianRect value) {
        this.mandelbrotSegment = value;
    }

    public int[] getBaseColor() {
        return paletteCariage.baseRGBColor;
    }

    public void setBaseColor(int red, int green, int blue) {
        paletteCariage.setBaseRGBColor(red, green, blue);
    }

    public boolean isRatioFixed() {
        return isRatioFixed;
    }

    public void setRatioFixed(boolean ratioFixed) {
        isRatioFixed = ratioFixed;
    }

    /*--- Constructors ---*/

    public MandelbrotView(Context context) {
        super(context);
    }

    public MandelbrotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MandelbrotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /*--- Private methods ---*/

    // Initialize the state of this view
    private void init() {
        if (mandelbrotSegment == null)
            setInitialSegment();
        paint = new Paint();
        paletteCariage = new PaletteCariage(INITIAL_RGB_COLOR[0], INITIAL_RGB_COLOR[1], INITIAL_RGB_COLOR[2]);
    }

    // Sets the initial segment
    private void setInitialSegment() {
        setMandelbrotSegment(isRatioFixed ? fixRatio(INITIAL_SEGMENT, true) : INITIAL_SEGMENT);
    }

    // Generates new bitmap asynchronously
    private void generateNewBitmap() {
        if (workingThreadState == WorkingThreadState.WORKING) {
            if (!mandelbrotBitmapGenerator.isCancelled()) {
                mandelbrotBitmapGenerator.cancel(false);
                isPendingRequest = true;
            }
            return;
        }

        MandelbrotBitmapGenerator.MandelbrotGeneratorParams params =
                new MandelbrotBitmapGenerator.MandelbrotGeneratorParams(dimensions, mandelbrotSegment,
                        paletteCariage.palette);
        refreshPalette();
        mandelbrotBitmapGenerator = new MandelbrotBitmapGenerator(this, getContext());
        mandelbrotBitmapGenerator.execute(params);
    }

    /* Regenerates the palette if it is marked as invalid. Note, that the palette array
    itself is a shared resource here, so an extra care is required to decide when it is safe to
    call this method. It is safe if no working thread runs, obviously. But it is also
    ok call this when all possible working threads are scheduled to be canceled - the palette
    will become inconsistent for them, but it does not matter as we will not use their
    result anyway.
     */
    private void refreshPalette() {
        if (!paletteCariage.isValid) {
            paletteCariage.generatePalette();
        }
    }

    // Fixes or stretches the ratio of the source cartesian rect according to this view's dimensions
    private CartesianRect fixRatio(CartesianRect sourceRect, boolean fix) {
        float centerY = sourceRect.top - sourceRect.height / 2;
        float height;
        if (fix) {
            float dimensionRatio = (float) dimensions.height() / dimensions.width();
            height = dimensionRatio * sourceRect.width;
        } else {
            float initialSegmentRatio = INITIAL_SEGMENT.height / INITIAL_SEGMENT.width;
            height = initialSegmentRatio * sourceRect.width;

        }

        return new CartesianRect(sourceRect.left, centerY + height / 2,
                sourceRect.right, centerY - height / 2);
    }

    /*--- View's callbacks ---*/

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        dimensions = new Rect(0, 0, w, h);
        centerX = w / 2;
        centerY = h / 2;
        // Make sure to keep the ratio fixed if it is desired so
        if (isRatioFixed()) {
            setMandelbrotSegment(fixRatio(mandelbrotSegment, true));
        }
        generateNewBitmap();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        /* Pokud jeste neni nakreslena bitmapa, tak ji nakreslime */
        if (currentBitmap == null) {
            canvas.drawColor(Color.BLACK);
            paint.setColor(Color.WHITE);
            paint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText(String.format("Drawing... %.0f%% complete.", 100 * progress), centerX, centerY, paint);
        } else {
            canvas.drawBitmap(currentBitmap, 0, 0, paint);
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != MotionEvent.ACTION_DOWN)
            return false;
        /* Only do something when the bitmap is displayed */
        if (currentBitmap == null)
            return true;

        /* Zoom twice and make the position of the touch center of the segment */
        float centerX = mandelbrotSegment.left + (motionEvent.getX(0) / dimensions.width()) * mandelbrotSegment.width;
        float centerY = mandelbrotSegment.top - (motionEvent.getY(0) / dimensions.height()) * mandelbrotSegment.height;
        float radiusX = mandelbrotSegment.width / 4;
        float radiusY = mandelbrotSegment.height / 4;

        float min = 10e-12f;

        CartesianRect updatedSegment = new CartesianRect(centerX - radiusX, centerY + radiusY,
                centerX + radiusX, centerY - radiusY);
        if ((updatedSegment.left + updatedSegment.width / dimensions.width()) - updatedSegment.left <= min ||
                -(updatedSegment.top - updatedSegment.height / dimensions.height()) + updatedSegment.top <= min) {
            if (zoomMessageToast != null)
                zoomMessageToast.cancel();
            zoomMessageToast = Toast.makeText(getContext(), "Maximum zoom reached", Toast.LENGTH_SHORT);
            zoomMessageToast.show();
        } else {
            setMandelbrotSegment(updatedSegment);
            generateNewBitmap();
        }
        return true;
    }

    /* Public methods */

    /**
     * Resets this view and redraws it.
     */
    public void resetView() {
        setInitialSegment();
        generateNewBitmap();
    }

    /**
     * Fixes or stretches the bitmap in this view and forces it to redraw, if needed.
     * @param fix
     */
    public void changeRatio(boolean fix) {
        if (isRatioFixed() == fix)
            return;
        setMandelbrotSegment(fixRatio(getMandelbrotSegment(), fix));
        setRatioFixed(fix);
        generateNewBitmap();

    }

    /**
     * Redraws the bitmap in the view with a newly randomly generated color palette.
     */
    public void recolor() {
        paletteCariage.setBaseRGBColor(random.nextInt(256), random.nextInt(256), random.nextInt(256));
        generateNewBitmap();
    }

    /**
     * Ask this view to send cancel request to its working thread, if it is currently running.
     * This is useful to call when the parent fragment/activity stops.
     */
    public void stop() {
        if (workingThreadState == WorkingThreadState.WORKING) {
            mandelbrotBitmapGenerator.cancel(false);
        }
    }

    /**
     * Ask this view to restart the working thread if it was previously cancelled due to
     * the need of resources. Combine this with the stop() method.
     */
    public void restartIfCancelled() {
        if (workingThreadState == WorkingThreadState.CANCELLED)
            generateNewBitmap();
    }

    /**
     * Attemps to share the currently rendered bitmap via an intent, but has no effect
     * if no bitmap is currently rendered.
     * @param context The context which cache directory is used to temporary save the bitmap
     *                and to send the intent.
     * @return true if the intent was constructed and sent successfully, false otherwise.
     */
    public MandelbrotBitmapShareResult shareBitmap(Context context) {
        if (currentBitmap == null) {
            return MandelbrotBitmapShareResult.NOT_READY;
        }

        return performShareBitmap(context, currentBitmap) ? MandelbrotBitmapShareResult.OK :
                MandelbrotBitmapShareResult.ERROR;
    }

    private boolean performShareBitmap(Context context, Bitmap bitmap) {
        try {
            File file = new File(context.getCacheDir(), "tmpBitmap.png");
            FileOutputStream fOut = new FileOutputStream(file, false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);

            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/png");
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private Uri performSaveBitmapToExt(String fileName, Bitmap bitmap) {
        File file;
        try {
            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), fileName);
            FileOutputStream fOut = new FileOutputStream(file, false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return Uri.fromFile(file);
    }

    private void addToGallery(Context context, Uri fileUri) {
        MediaScannerConnection.scanFile(context, new String[]{fileUri.getPath()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String s, Uri uri) {
                    }
                });
    }

    private String generateFileName(Context context) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return context.getResources().getString(R.string.app_name) + "_" + timeStamp + ".png";
    }

    /**
     * Saves the currently rendered bitmap to the public external storage and adds it to the
     * image gallery.
     * @param context
     * @return
     */
    public MandelbrotBitmapShareResult saveBitmap(Context context) {
        if (currentBitmap == null)
            return MandelbrotBitmapShareResult.NOT_READY;
        Uri fileUri = performSaveBitmapToExt(generateFileName(context), currentBitmap);
        if (fileUri != null) {
            addToGallery(context, fileUri);
            return MandelbrotBitmapShareResult.OK;
        }
        return MandelbrotBitmapShareResult.ERROR;
    }



    public enum MandelbrotBitmapShareResult {OK, NOT_READY, ERROR}

    /*--- MandelbrotGeneratorListener implementation */

    @Override
    public void onPreExecute() {
        workingThreadState = WorkingThreadState.WORKING;
        if (currentBitmap != null)
            currentBitmap.recycle();
        currentBitmap = null;
    }

    @Override
    public void onUpdate(float progress) {
        this.progress = progress;
        invalidate();
    }

    @Override
    public void onPostExecute(Bitmap result) {
        currentBitmap = result;
        invalidate();
        workingThreadState = WorkingThreadState.INACTIVE;
        processPendingRequests();

    }

    @Override
    public void onCancelled(Bitmap result) {
        if (result != null)
            result.recycle();
        currentBitmap = null;
        progress = 0.0f;
        workingThreadState = WorkingThreadState.CANCELLED;
        processPendingRequests();
    }

    private void processPendingRequests() {
        if (isPendingRequest)
            generateNewBitmap();
        isPendingRequest = false;
    }

    /*--- Helper classes and carriages ---*/

    public enum WorkingThreadState {INACTIVE, CANCELLED, WORKING}

    private class PaletteCariage {
        public final int[] palette;
        public int[] baseRGBColor;
        public boolean isValid;

        public PaletteCariage(int baseRed, int baseGreen, int baseBlue) {
            this.palette = new int[PALETTE_SIZE];
            this.baseRGBColor = new int[]{baseRed, baseGreen, baseBlue};
            this.isValid = false;
        }

        public void setBaseRGBColor(int baseRed, int baseGreen, int baseBlue) {
            baseRGBColor[0] = baseRed;
            baseRGBColor[1] = baseGreen;
            baseRGBColor[2] = baseBlue;
            isValid = false;
        }

        public void generatePalette() {
            PaletteGenerator.createPalette(baseRGBColor[0], baseRGBColor[1], baseRGBColor[2],
                    palette);
            isValid = true;
        }
    }



}
